package com.example.demo.ratelimiter;

import com.google.common.util.concurrent.RateLimiter;

/**
 * @author Created by niugang on 2020/3/25/14:34
 */
public class Test1 {

    public static void main(String[] args) {
        RateLimiter rateLimiter = RateLimiter.create(5);
        System.out.println(rateLimiter.acquire());
        System.out.println(rateLimiter.acquire());
        System.out.println(rateLimiter.acquire());
        System.out.println(rateLimiter.acquire());
        System.out.println(rateLimiter.acquire());
        System.out.println(rateLimiter.acquire());

    }
}

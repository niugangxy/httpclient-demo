package com.example.demo.httpclient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * http://hc.apache.org/httpcomponents-client-ga/examples.html
 *
 * @author Created by niugang on 2020/3/24/11:06
 * 此示例演示如何使用响应处理程序处理HTTP响应。
 * 这是执行HTTP请求和处理HTTP响应的推荐方法。
 * 这种方法使调用者可以专注于摘要HTTP响应的过程，并将系统资源释放任务委派给HttpClient。
 * HTTP响应处理程序的使用保证了在所有情况下基础HTTP连接都会自动释放回连接管理器。
 */
public class ClientWithResponseHandler {


    public static void main(String[] args) throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet("http://11.12.115.104:8080/serverStatus/serverInfo");

            System.out.println("Executing request " + httpget.getRequestLine());

            // 自定义响应处理器
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            String responseBody = httpclient.execute(httpget, responseHandler);
            System.out.println("----------------------------------------");
            //打印响应内容
            System.out.println(responseBody);
        } finally {
            httpclient.close();
        }
    }
}

package com.example.demo.httpclient;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.IOException;

/**
 * https post请求  请求体 为json数据参数
 *
 * @author Created by niugang on 2020/3/25/8:52
 */
@Slf4j
public class ClientCustomSSL3 {
    public static void main(String[] args) throws Exception {


        SSLContext sslContext = SSLContexts.custom()
                .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                .build();
        // 该主机名验证程序实际上关闭了主机名验证。 它接受任何有效的SSL会话并匹配目标主机
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        CloseableHttpClient httpclient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .build();

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user", "22222");
            jsonObject.put("ip", "192.168.1.2");
            jsonObject.put("status", "0");
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(jsonObject.toJSONString().getBytes(), ContentType.create("application/json", "UTF-8"));

            HttpPost httpget = new HttpPost("https://11.12.115.104/api/accessControl/saveDevice.do");
            httpget.setHeader("token", "d65d7287e3b060265040148c457a821b");
            httpget.setEntity(byteArrayEntity);

            log.info("Executing request {}", httpget.getRequestLine());

            // 自定义响应处理器
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status + "->" + response.getStatusLine().getReasonPhrase());
                    }
                }

            };
            String responseBody = httpclient.execute(httpget, responseHandler);
            log.info("----------------------------------------");
            //打印响应内容
            log.info(responseBody);
        } finally {
            httpclient.close();
        }
    }


}

package com.example.demo.httpasyncclient;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * http://hc.apache.org/httpcomponents-asyncclient-dev/examples.html
 * @author Created by niugang on 2020/3/24/8:37
 */
public class AsyncTest {


    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {

        CloseableHttpAsyncClient httpclient = HttpAsyncClients.createDefault();
        try {
            // Start the client
            httpclient.start();

            // 执行请求
            final HttpGet request1 = new HttpGet("http://www.baicom.com/");
            Future<HttpResponse> future = httpclient.execute(request1, null);
            // 等到收到回应
            HttpResponse response1 = future.get();

            System.out.println(request1.getRequestLine() + "->" + response1.getStatusLine());


        } finally {
            httpclient.close();
        }


    }
}
